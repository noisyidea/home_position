# CameraControl (Home Position)

This project uses Carthage dependency manager to load 3rd party frameworks. It must be installed in order to build the project. Follow the provided steps to install via HomeBrew:

### Ensure HomeBrew Is Installed
- Run `$ brew --version` in terminal.
- You'll see something like `Homebrew 1.2.4` if it's installed.
- If not, go to https://brew.sh for installation instructions.

### Install Carthage
- With HomeBrew installed run `brew update` in terminal.
- Run `brew install carthage`.

### Install Dependencies
- With Carthage installed run `carthage bootstrap --platform iOS` in terminal.

## Configuration
- Build configurations are set in Xcode by selecting the appropriate Scheme in top left corner. CameraControl should probably always be selected.

## Application Architecture
- This app follows the Coordinator pattern as described by Soroush Khanlou here: http://khanlou.com/2015/10/coordinators-redux/.
