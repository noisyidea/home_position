import Foundation

extension String {
    /// Confirm whether a non-nil URL instance can be created. No additional validation performed.
    var isValidURL: Bool {
        return self.asURL != nil
    }
    
    var asURL: URL? {
        return URL(string: self)
    }
}
