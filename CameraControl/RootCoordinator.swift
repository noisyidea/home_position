import UIKit

// MARK: - RootCoordinator

class RootCoordinator {
    
    // MARK: - Stored Properties
    
    fileprivate let rootViewController: UIViewController
    fileprivate let networkProvider: NetworkProvider
    
    // MARK: - Child Coordinators
    
    private var cameraControlCoordinator: CameraControlCoordinator?
    
    // MARK: - Lifecycle
    
    init(
        rootViewController: UIViewController,
        networkProvider: NetworkProvider)
    {
        self.rootViewController = rootViewController
        self.networkProvider = networkProvider
    }
    
    func start() {
        do { try showCameraControl() }
        catch { rootViewController.presentAlert(forUnrecoverableError: error) }
    }
    
    // MARK: - Navigation
    
    private func showCameraControl() throws {
        cameraControlCoordinator = try CameraControlCoordinator(
            delegate: self,
            rootViewController: rootViewController,
            networkProvider: networkProvider
        )
        
        cameraControlCoordinator?.start()
    }
}

// MARK: - CameraControlCoordinatorDelegate

extension RootCoordinator: CameraControlCoordinatorDelegate {}
