import Foundation

// MARK: - NoisyIdeaNetworkProvider

class NoisyIdeaNetworkProvider: NetworkProvider {
    
    // MARK: - Stored Properties
    
    private let defaults = UserDefaults.standard
    
    private let defaultServerHostAddress = "192.168.0.72"
    private let defaultVideoHostAddress = "192.168.0.70"
    
    // MARK: - DefaultsKey definitions
    
    private enum DefaultsKey {
        static let serverHostAddress = "NoisyIdea.serverHostAddress"
        static let videoHostAddress = "NoisyIdea.videoHostAddress"
    }
    
    // MARK: - Addresses
    
    var serverHostAddress: String {
        get { return defaults.string(forKey: DefaultsKey.serverHostAddress) ?? defaultServerHostAddress }
        set {
            guard newValue != serverHostAddress else { return }
            
            defaults.set(newValue, forKey: DefaultsKey.serverHostAddress)
            NotificationCenter.default.post(name: .serverURLDidChange, object: nil)
        }
    }
    
    var videoHostAddress: String {
        get { return defaults.string(forKey: DefaultsKey.videoHostAddress) ?? defaultVideoHostAddress }
        set {
            guard newValue != videoHostAddress else { return }
            
            defaults.set(newValue, forKey: DefaultsKey.videoHostAddress)
            NotificationCenter.default.post(name: .videoURLDidChange, object: nil)
        }
    }
    
    // MARK: - URLs
    
    var serverURL: URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = serverHostAddress
        urlComponents.port = 80
        urlComponents.path = ""
        
        return urlComponents.url
    }
    
    var videoURL: URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = videoHostAddress
        urlComponents.port = 5001
        urlComponents.path = "/video_feed"
        
        return urlComponents.url
    }
}
