import UIKit

// MARK: - CameraControlCoordinatorDelegate

protocol CameraControlCoordinatorDelegate: class {
}

// MARK: - CameraControlCoordinator

class CameraControlCoordinator {
    
    // MARK: - Stored Properties
    
    fileprivate weak var delegate: CameraControlCoordinatorDelegate?
    fileprivate let rootViewController: UIViewController
    fileprivate var networkProvider: NetworkProvider
    
    fileprivate var cameraControlSession: CameraControlSession?
    
    fileprivate var videoPlayerVC: VideoPlayerViewController?
    fileprivate var cameraControlVC: CameraControlViewController?
    
    // MARK: - Lifecycle
    
    init(
        delegate: CameraControlCoordinatorDelegate,
        rootViewController: UIViewController,
        networkProvider: NetworkProvider) throws
    {
        self.delegate = delegate
        self.rootViewController = rootViewController
        self.networkProvider = networkProvider
        
        self.cameraControlSession = try CameraControlSession(
            networkProvider: networkProvider
        )
    }
    
    func start() {
        showCameraControl()
    }
    
    // MARK: - Navigation
    
    private func showCameraControl() {
        // VideoPlayerVC
        let videoPlayerVC = VideoPlayerViewController.loadFromStoryboard()
        self.videoPlayerVC = videoPlayerVC
        
        videoPlayerVC.inject(
            dependencies: .init(networkProvider: networkProvider)
        )
        
        // CameraControlVC
        let cameraControlVC = CameraControlViewController.loadFromStoryboard()
        self.cameraControlVC = cameraControlVC
        
        cameraControlVC.embed(
            childViewController: videoPlayerVC,
            inContainer: .videoPlayer
        )
        
        cameraControlVC.update(with: .init(
            serverHostAddress: networkProvider.serverHostAddress,
            videoHostAddress: networkProvider.videoHostAddress
        ))
        
        cameraControlVC.delegate = self
        
        // Present
        rootViewController.present(cameraControlVC, animated: false) {
            videoPlayerVC.play()
        }
    }
}

// MARK: - CameraControlViewControllerDelegate

extension CameraControlCoordinator: CameraControlViewControllerDelegate {
    func cameraControlViewController(
        _: CameraControlViewController,
        didUpdateServerHostAddress serverHostAddress: String)
    {
        networkProvider.serverHostAddress = serverHostAddress
        resetCameraControlSession()
    }
    
    func cameraControlViewController(
        _: CameraControlViewController,
        didUpdateVideoHostAddress videoHostAddress: String)
    {
        networkProvider.videoHostAddress = videoHostAddress
        resetCameraControlSession()
    }
    
    fileprivate func resetCameraControlSession() {
        do {
            cameraControlSession = try CameraControlSession(
                networkProvider: networkProvider
            )
        } catch {
            self.cameraControlVC?.presentAlert(for: error)
        }
    }
    
    func cameraControlViewControllerDidSelectSave(
        _: CameraControlViewController)
    {
        cameraControlSession?.savePosition(resultQueue: .main) { result in
            switch result {
            case .failure(let error):
                self.cameraControlVC?.presentAlert(for: error)
                
            case .success:
                self.cameraControlVC?.presentOKAlert(title: "Saved successfully!")
            }
        }
    }
    
    func cameraControlViewController(
        _: CameraControlViewController,
        didSelectCameraAdjustment adjustment: CameraAdjustment)
    {
        cameraControlSession?.adjustCamera(by: adjustment, resultQueue: .main) { result in
            switch result {
            case .failure(let error):
                self.cameraControlVC?.presentAlert(for: error)
                
            case .success:
                print("\(#function) - .success")
            }
        }
    }
}
