import Foundation
import JFCToolKit
import SwiftyZeroMQ

class CameraControlConnection {
    
    // MARK: - Related Types
    
    enum Response: String {
        case ok = "OK"
        case failed = "FAILED"
        case timeout = "TIMEOUT"
        
        init(status: String) throws {
            guard let response = Response(rawValue: status) else {
                throw ResponseError.couldNotCreateResponse(fromStatus: status)
            }
            
            self = response
        }
    }
    
    enum ResponseError: Error, CustomStringConvertible {
        case couldNotCreateResponse(fromStatus: String)
        case failed
        case timedOut
        
        var description: String {
            switch self {
            case .couldNotCreateResponse:
                return "\(self)"
            case .failed:
                return "Server responded with \(Response.failed.rawValue) response code."
            case .timedOut:
                return "Server responded with \(Response.timeout.rawValue) response code."
            }
        }
    }
    
    enum ConnectionError: Error {
        case receivedNilReplyFromServer
    }
    
    // MARK: - Stored Properties
    
    private let networkProvider: NetworkProvider
    private let context: SwiftyZeroMQ.Context
    private var requestFromServerSocket: SwiftyZeroMQ.Socket?
    
    // MARK: - Lifecycle
    
    init(networkProvider: NetworkProvider) throws {
        self.networkProvider = networkProvider
        self.context = try SwiftyZeroMQ.Context()
    }
    
    deinit {
        shutDownEverything()
    }
    
    func shutDownEverything() {
        do {
            try self.context.shutdown()
            try self.context.terminate()
        } catch {
            assertionFailure(error.localizedDescription)
        }
    }
    
    // MARK: - Setup Sockets
    
    func setupRequestFromServerSocket() throws {
        requestFromServerSocket = try context.socket(.request)
        let replyEndpoint = "tcp://\(networkProvider.serverHostAddress):5554"
        try requestFromServerSocket?.connect(replyEndpoint)
    }
    
    // MARK: - Send
    
    @discardableResult
    func sendRequest(withJSONDictionary jsonDictionary: [String: Any]) throws -> CameraControlConnection {
        let jsonData = try JSONSerialization.data(
            withJSONObject: jsonDictionary,
            options: []
        )
        
        try requestFromServerSocket?.send(data: jsonData)
        return self
    }
    
    // MARK: - Receive
    
    /// Returns response unless failure or timeout occurs in which case the appropriate error is thrown.s
    @discardableResult
    func waitForReply() throws -> Response {
        guard let data = try requestFromServerSocket?.recvMultipart().first else {
            throw ConnectionError.receivedNilReplyFromServer
        }
        
        let jsonDictionary = try JSONParser.parseRootObject(from: data) as [String: Any]
        let status = try JSONParser.parse(key: "status", from: jsonDictionary) as String
        
        let response = try Response(status: status)
        
        switch response {
        case .failed: throw ResponseError.failed
        case .timeout: throw ResponseError.timedOut
        case .ok: return response
        }
    }
}
