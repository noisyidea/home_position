import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var rootCoordinator: RootCoordinator?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        let rootViewController = UIViewController()
        
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
        
        rootCoordinator = RootCoordinator(
            rootViewController: rootViewController,
            networkProvider: networkProviderForCurrentConfiguration()
        )
        rootCoordinator?.start()
        
        return true
    }
    
    private func networkProviderForCurrentConfiguration() -> NetworkProvider {
        #if JEFFREY_LOCAL
            return JeffreyNetworkProvider()
        #else
            return NoisyIdeaNetworkProvider()
        #endif
    }
}

