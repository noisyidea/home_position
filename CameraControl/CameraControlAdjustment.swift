enum CameraAdjustment {
    case panClockwise(speed: Int)
    case panCounterClockwise(speed: Int)
    case tiltUp, tiltDown
    
    var command: String {
        switch self {
        case .panClockwise, .panCounterClockwise: return "PAN"
        case .tiltUp, .tiltDown: return "TILT"
        }
    }
    
    var direction: String {
        switch self {
        case .panClockwise: return "CW"
        case .panCounterClockwise: return "CCW"
        case .tiltUp: return "UP"
        case .tiltDown: return "DOWN"
        }
    }
    
    var speed: String? {
        switch self {
        case .panClockwise(let speed): return "\(speed)"
        case .panCounterClockwise(let speed): return "\(speed)"
        case .tiltUp, .tiltDown: return nil
        }
    }
}

