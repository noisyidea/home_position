import UIKit
import JFCToolKit

// MARK: - CameraControlViewData

struct CameraControlViewData {
    let serverHostAddress: String
    let videoHostAddress: String
}

// MARK: - CameraControlViewControllerDelegate

protocol CameraControlViewControllerDelegate: class {
    func cameraControlViewController(
        _: CameraControlViewController,
        didUpdateServerHostAddress serverHostAddress: String
    )
    
    func cameraControlViewController(
        _: CameraControlViewController,
        didUpdateVideoHostAddress videoHostAddress: String
    )
    
    func cameraControlViewControllerDidSelectSave(
        _: CameraControlViewController
    )
    
    func cameraControlViewController(
        _: CameraControlViewController,
        didSelectCameraAdjustment adjustment: CameraAdjustment
    )
}

// MARK: - LoadableFromStoryboard

extension CameraControlViewController: LoadableFromStoryboard {
    static let storyboardFilename = "CameraControl"
}

// MARK: - CameraControlViewController

class CameraControlViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate var videoPlayerContainerView: UIView!
    
    @IBOutlet var serverTextField: UITextField!
    @IBOutlet var videoTextField: UITextField!
    
    @IBOutlet var tiltBlurContainer: UIVisualEffectView!
    @IBOutlet var rotateBlurContainer: UIVisualEffectView!
    
    @IBOutlet var panSpeedLabel: UILabel!
    @IBOutlet var panSpeedSlider: VerticalSlider!
    
    // MARK: - Stored Properties
    
    weak var delegate: CameraControlViewControllerDelegate?
    
    var embeddedChildVCs: [ChildViewControllerContainer: UIViewController] = [:]
    
    // MARK: - Computed Properties
    
    private var panSpeed: Int = 1 {
        didSet {
            guard panSpeed != oldValue else { return }
            panSpeedLabel.text = "\(panSpeed)x"
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
    }
    
    // MARK: - Setup
    
    private func setupViews() {
        setupTextFields()
        setupBlurContainers()
        setupPanSpeedSlider()
    }
    
    private func setupTextFields() {
        serverTextField.delegate = self
        videoTextField.delegate = self
    }
    
    private func setupBlurContainers() {
        [tiltBlurContainer, rotateBlurContainer].forEach { blurContainer in
            blurContainer?.layer.cornerRadius = 30
            blurContainer?.layer.masksToBounds = true
        }
    }
    
    private func setupPanSpeedSlider() {
        self.panSpeedSlider.value = Float(panSpeed)
        self.panSpeedSlider.slider.addTarget(
            self,
            action: #selector(panSpeedSliderValueChanged),
            for: .valueChanged
        )
    }
    
    // MARK: - Update
    
    func update(with viewData: CameraControlViewData) {
        serverTextField.text = viewData.serverHostAddress
        videoTextField.text = viewData.videoHostAddress
    }
    
    // MARK: - Actions
    
    @IBAction func savePositionTapped(_ sender: Any) {
        delegate?.cameraControlViewControllerDidSelectSave(self)
    }
    
    @IBAction func upTapped(_ sender: Any) {
        delegate?.cameraControlViewController(self, didSelectCameraAdjustment: .tiltUp)
    }
    
    @IBAction func downTapped(_ sender: Any) {
        delegate?.cameraControlViewController(self, didSelectCameraAdjustment: .tiltDown)
    }
    
    @IBAction func clockwiseTapped(_ sender: Any) {
        delegate?.cameraControlViewController(
            self,
            didSelectCameraAdjustment: .panClockwise(speed: panSpeed)
        )
    }
    
    @IBAction func counterClockwiseTapped(_ sender: Any) {
        delegate?.cameraControlViewController(
            self,
            didSelectCameraAdjustment: .panCounterClockwise(speed: panSpeed)
        )
    }
    
    func panSpeedSliderValueChanged() {
        // Round to nearest value which creates the stepping effect.
        let rounded = round(panSpeedSlider.value)
        panSpeedSlider.value = rounded
        panSpeed = Int(rounded)
    }
}

// MARK: - ChildViewControllerEmbeddable

extension CameraControlViewController: ChildViewControllerEmbeddable {
    enum ChildViewControllerContainer: String {
        case videoPlayer
    }
    
    func containerView(
        for container: ChildViewControllerContainer) -> UIView
    {
        self.loadViewIfNeeded()
        
        switch container {
        case .videoPlayer: return self.videoPlayerContainerView
        }
    }
}

// MARK: - UITextFieldDelegate

extension CameraControlViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return textField.textIsValidURL
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        switch textField {
        case self.serverTextField:
            delegate?.cameraControlViewController(
                self,
                didUpdateServerHostAddress: textField.text ?? ""
            )
        case self.videoTextField:
            delegate?.cameraControlViewController(
                self,
                didUpdateVideoHostAddress: textField.text ?? ""
            )
        default:
            break
        }
    }
}
