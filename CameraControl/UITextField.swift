import UIKit

extension UITextField {
    /// Confirm whether a non-nil URL instance can be created. No additional validation performed.
    var textIsValidURL: Bool {
        return self.asURL != nil
    }
    
    var asURL: URL? {
        let urlString = self.text ?? ""
        return urlString.asURL
    }
}
