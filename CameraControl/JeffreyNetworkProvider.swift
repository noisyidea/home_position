import UIKit

// MARK: - JeffreyNetworkProvider

class JeffreyNetworkProvider: NetworkProvider {
    
    // MARK: - Stored Properties
    
    private let defaults = UserDefaults.standard
    
    private let defaultServerHostAddress = "192.168.20.213"
    private let defaultVideoHostAddress = "192.168.20.213"
    
    // MARK: - DefaultsKey definitions
    
    private enum DefaultsKey {
        static let serverHostAddress = "Jeffrey.serverHostAddress"
        static let videoHostAddress = "Jeffrey.videoHostAddress"
    }
    
    // MARK: - Addresses
    
    var serverHostAddress: String {
        get { return defaults.string(forKey: DefaultsKey.serverHostAddress) ?? defaultServerHostAddress }
        set {
            defaults.set(newValue, forKey: DefaultsKey.serverHostAddress)
            NotificationCenter.default.post(name: .serverURLDidChange, object: nil)
        }
    }
    
    var videoHostAddress: String {
        get { return defaults.string(forKey: DefaultsKey.videoHostAddress) ?? defaultVideoHostAddress }
        set {
            defaults.set(newValue, forKey: DefaultsKey.videoHostAddress)
            NotificationCenter.default.post(name: .videoURLDidChange, object: nil)
        }
    }
    
    // MARK: - URLs
    
    var serverURL: URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = serverHostAddress
        urlComponents.port = 80
        urlComponents.path = ""
        
        return urlComponents.url
    }
    
    var videoURL: URL? {
//        return URL(string: "http://195.67.26.73/mjpg/video.mjpg")
        
        var urlComponents = URLComponents()
        urlComponents.scheme = "http"
        urlComponents.host = videoHostAddress
        urlComponents.port = 80
        urlComponents.path = ""
        
        return urlComponents.url
    }
}
