import UIKit
import JFCToolKit

// MARK: - LoadableFromStoryboard

extension VideoPlayerViewController: LoadableFromStoryboard {
    static var storyboardFilename: String { return "VideoPlayer" }
}

// MARK: - DependencyInjectable

extension VideoPlayerViewController: DependencyInjectable {
    struct Dependencies {
        var networkProvider: NetworkProvider
    }
    
    func inject(dependencies: Dependencies) {
        self.networkProvider = dependencies.networkProvider
    }
}

// MARK: - VideoPlayerViewController

class VideoPlayerViewController: UIViewController {
    
    // MARK: - Dependencies
    
    fileprivate var networkProvider: NetworkProvider!
    
    // MARK: - Outlets
    
    @IBOutlet fileprivate var mjpegImageView: UIImageView!
    
    // MARK: - Stored Properties
    
    private var videoURL: URL? { return networkProvider.videoURL }
    private var streamingController: MjpegStreamingController!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupStreamingController()
        registerForNotifications()
    }
    
    deinit {
        deregisterForNotifications()
    }
    
    // MARK: - Notifications
    
    private func registerForNotifications() {
        NotificationCenter.default.addObserver(
            self, selector:
            #selector(updateStreamingController),
            name: .videoURLDidChange,
            object: nil
        )
    }
    
    private func deregisterForNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Setup
    
    private func setupStreamingController() {
        self.streamingController = MjpegStreamingController(
            imageView: self.mjpegImageView,
            contentURL: videoURL!
        )
    }
    
    // MARK: - Update
    
    @objc
    private func updateStreamingController() {
        stop()
        streamingController.contentURL = networkProvider.videoURL
        play()
    }
    
    // MARK: - Actions
    
    func play() {
        // Load views if called externally before the views have loaded.
        self.loadViewIfNeeded()
        
        streamingController.play()
    }
    
    func stop() {
        streamingController.stop()
    }
    
    func togglePlayPause() {
        // Load views if called externally before the views have loaded.
        self.loadViewIfNeeded()
        
        streamingController.togglePlayPause()
    }
}
