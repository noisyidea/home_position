import Foundation

extension Notification.Name {
    static let serverURLDidChange = Notification.Name("serverURLDidChange")
    static let videoURLDidChange = Notification.Name("videoURLDidChange")
}
