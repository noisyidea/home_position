import UIKit

extension UIViewController {
    
    // MARK: - Alerts
    
    func presentOKAlert(
        title: String,
        message: String? = nil,
        completion: (() -> Void)? = nil)
    {
        let alertController = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        let okAction = UIAlertAction(title: "OK", style: .default) { _ in completion?() }
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true)
    }
    
    // MARK: Error Alerts
    
    func presentAlert(for error: Error) {
        self.presentAlert(for: error, withTitle: "Error")
    }
    
    func presentAlert(forRecoverableError error: Error) {
        self.presentAlert(for: error, withTitle: "Recoverable Error")
    }
    
    func presentAlert(forUnrecoverableError error: Error) {
        self.presentAlert(for: error, withTitle: "Unrecoverable Error")
    }
    
    private func presentAlert(for error: Error, withTitle title: String) {
        let alertController = UIAlertController(
            title: title,
            message: "\(error)",
            preferredStyle: .alert
        )
        
        let okAction = UIAlertAction(title: "Ok", style: .default)
        alertController.addAction(okAction)
        
        self.present(alertController, animated: true)
    }
}
