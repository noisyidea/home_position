import Foundation
import JFCToolKit
import SwiftyZeroMQ

// MARK: - CameraControlSession

class CameraControlSession {
    
    // MARK: - Related Types
    
    enum State {
        case disconnected
        case connecting
        case connected(pan: Int, tilt: Int)
        
        func adjusted(by adjustment: CameraAdjustment) -> State {
            switch self {
            case .disconnected, .connecting:
                return self
                
            case .connected(let currentPan, let currentTilt):
                switch adjustment {
                case .panClockwise(let speed):
                    // Multiply by 4 because Bill said so.
                    let incrementBy = 4 * speed
                    
                    return .connected(
                        pan: (currentPan + incrementBy).wrapped(at: 0, and: 2000),
                        tilt: currentTilt
                    )
                case .panCounterClockwise(let speed):
                    // Multiply by 4 because Bill said so.
                    let decrementBy = 4 * speed
                    
                    return .connected(
                        pan: (currentPan - decrementBy).wrapped(at: 0, and: 2000),
                        tilt: currentTilt
                    )
                case .tiltUp:
                    return .connected(
                        pan: currentPan,
                        tilt: (currentTilt + 4).clamped(by: 0...900)
                    )
                case .tiltDown:
                    return .connected(
                        pan: currentPan,
                        tilt: (currentTilt - 4).clamped(by: 0...900)
                    )
                }
            }
        }
    }
    
    enum Command {
        case `init`
        case adjustCamera(by: CameraAdjustment)
        case savePosition(pan: Int, tilt: Int)
        
        var command: String {
            switch self {
            case .init: return "INIT"
            case .adjustCamera(let adjustment): return adjustment.command
            case .savePosition: return "SAVE"
            }
        }
        
        func jsonDictionary() throws -> [String: Any] {
            var jsonDictionary: [String: Any] = [
                "command": command
            ]
            
            switch self {
            case .init:
                break
                
            case .adjustCamera(let adjustment):
                jsonDictionary["data"] = adjustment.direction
                
                if let speed = adjustment.speed {
                    jsonDictionary["speed"] = speed
                }
            case .savePosition(let pan, let tilt):
                let formatter = NumberFormatter()
                formatter.minimumIntegerDigits = 3
                formatter.maximumIntegerDigits = 3
                
                guard let formattedPan = formatter.string(for: pan) else {
                    throw CommandError.failedToFormatPan(pan: pan)
                }
                
                guard let formattedTilt = formatter.string(for: tilt) else {
                    throw CommandError.failedToFormatTilt(tilt: tilt)
                }
                
                jsonDictionary["data"] = [
                    "pan": formattedPan,
                    "tilt": formattedTilt
                ]
            }
            
            return jsonDictionary
        }
    }
    
    enum CommandError: Error {
        case invalid(command: Command, during: State)
        case failedToFormatPan(pan: Int)
        case failedToFormatTilt(tilt: Int)
    }
    
    // MARK: - Stored Properties
    
    private let connection: CameraControlConnection
    private var state: State = .disconnected
    
    // MARK: - Lifecycle
    
    init(networkProvider: NetworkProvider) throws {
        self.connection = try CameraControlConnection(
            networkProvider: networkProvider
        )
    }
    
    // MARK: - Commands
    
    func adjustCamera(
        by adjustment: CameraAdjustment,
        resultQueue: OperationQueue,
        resultHandler: @escaping (AsyncResult<Void>) -> Void)
    {
        DispatchQueue.global().async {
            switch self.state {
            case .connecting:
                // Ignore incoming commands.
                let error = CommandError.invalid(
                    command: .adjustCamera(by: adjustment),
                    during: self.state
                )
                resultQueue.addOperation { resultHandler(.failure(error)) }
                return
            case .disconnected:
                // Connect and retry command.
                do {
                    self.state = .connecting
                    
                    try self.connection.setupRequestFromServerSocket()
                    try self.perform(command: .init)
                    
                    self.adjustCamera(
                        by: adjustment,
                        resultQueue: resultQueue,
                        resultHandler: resultHandler
                    )
                } catch {
                    self.state = .disconnected
                    resultQueue.addOperation { resultHandler(.failure(error)) }
                    return
                }
            case .connected:
                // Perform command.
                do {
                    try self.perform(command: .adjustCamera(by: adjustment))
                    resultQueue.addOperation { resultHandler(.success() ) }
                    return
                } catch {
                    resultQueue.addOperation { resultHandler(.failure(error)) }
                    return
                }
            }
        }
    }
    
    func savePosition(
        resultQueue: OperationQueue,
        resultHandler: @escaping (AsyncResult<Void>) -> Void)
    {
        DispatchQueue.global().async {
            switch self.state {
            case .connecting, .disconnected:
                let error = CommandError.invalid(
                    command: .savePosition(pan: 0, tilt: 0),
                    during: self.state
                )
                resultQueue.addOperation { resultHandler(.failure(error)) }
                
            case .connected(let pan, let tilt):
                do {
                    try self.perform(command: .savePosition(pan: pan, tilt: tilt))
                    resultQueue.addOperation { resultHandler(.success() ) }
                } catch {
                    resultQueue.addOperation { resultHandler(.failure(error)) }
                }
            }
        }
    }
    
    private func perform(command: Command) throws {
        let jsonDictionary = try command.jsonDictionary()
        try connection
            .sendRequest(withJSONDictionary: jsonDictionary)
            .waitForReply()
        
        switch command {
        case .init:
            state = .connected(pan: 0, tilt: 0)
            
        case .adjustCamera(let adjustment):
            state = state.adjusted(by: adjustment)
            
        case .savePosition:
            state = .disconnected
        }
    }
}
