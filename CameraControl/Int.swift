import Foundation

extension CountableClosedRange {
    func clamp(_ value : Bound) -> Bound {
        return self.lowerBound > value ? self.lowerBound
            : self.upperBound < value ? self.upperBound
            : value
    }
}

extension Int {
    func clamped(by clampingRange: CountableClosedRange<Int>) -> Int {
        return clampingRange.clamp(self)
    }
    
    func wrapped(at lowerBound: Int, and upperBound: Int) -> Int {
        if (lowerBound..<upperBound).contains(self) {
            return self
        } else {
            return abs(upperBound - abs(self - lowerBound)) % upperBound
        }
    }
}
