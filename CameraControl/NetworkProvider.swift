import Foundation

protocol NetworkProvider {
    var serverHostAddress: String { get set }
    var videoHostAddress: String { get set }
    
    // Computed from Host Addresses above
    var serverURL: URL? { get }
    var videoURL: URL? { get }
}
