import UIKit
import SwiftyZeroMQ

class ServerVC: UIViewController {
    
    // MARK: - Stored Properties
    
    let replyToClientEndpoint = "tcp://*:5554"
    
    let context = try! SwiftyZeroMQ.Context()
    var replyToClientSocket: SwiftyZeroMQ.Socket?
    
    // MARK: - Outlets
    
    @IBOutlet var imageView: UIImageView!
    
    // MARK: - Lifecycle

    override func viewDidAppear(_ animated: Bool) {
        try! self.setupReplyToClientSocket()
        self.listenForRequest()
    }
    
    // MARK: - Setup
    
    private func setupReplyToClientSocket() throws {
        replyToClientSocket = try context.socket(.reply)
        try replyToClientSocket?.bind(replyToClientEndpoint)
    }
    
    // MARK: - Receive/Send
    
    private func listenForRequest() {
        DispatchQueue.global().async {
            do {
                let data = try self.replyToClientSocket!.recvMultipart().first!
                let jsonDictionary = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                
                let command = jsonDictionary["command"] as! String
                
                switch command {
                case "INIT":
                    self.presentCommandReceivedAlert(
                        command: command,
                        message: "n/a"
                    )
                case "PAN":
                    let direction = jsonDictionary["data"] as! String
                    let speed = jsonDictionary["speed"] as! String
                    self.presentCommandReceivedAlert(
                        command: command,
                        message: "data: \(direction)\nspeed: \(speed)"
                    )
                case "TILT":
                    let direction = jsonDictionary["data"] as! String
                    self.presentCommandReceivedAlert(
                        command: command,
                        message: "data: \(direction)"
                    )
                case "SAVE":
                    let dataDictionary = jsonDictionary["data"] as! [String: Any]
                    let pan = dataDictionary["pan"] as! String
                    let tilt = dataDictionary["tilt"] as! String
                    
                    self.presentCommandReceivedAlert(
                        command: command,
                        message: "pan: \(pan), tilt: \(tilt)"
                    )
                default:
                    print("No known actions for received command: \(command)")
                }
            } catch {
                print(error)
            }
        }
    }
    
    private func presentCommandReceivedAlert(command: String, message: String) {
        DispatchQueue.main.async {
            let alertController = UIAlertController(
                title: "\(command) Command Received",
                message: message,
                preferredStyle: .alert
            )
            
            let failAction = UIAlertAction(
                title: "Fail",
                style: .default,
                handler: { _ in
                    self.reply(status: "FAILED", notification: "for command: \(command)")
                }
            )
            
            let timeoutAction = UIAlertAction(
                title: "Timeout",
                style: .default,
                handler: { _ in
                    self.reply(status: "TIMEOUT", notification: "for command: \(command)")
                }
            )
            
            let okAction = UIAlertAction(
                title: "OK",
                style: .default,
                handler: { _ in
                    self.reply(status: "OK", notification: "okAction selected")
                }
            )
            
            alertController.addAction(okAction)
            alertController.addAction(failAction)
            alertController.addAction(timeoutAction)
            
            self.present(alertController, animated: true)
        }
    }
    
    private func reply(status: String, notification: String) {
        DispatchQueue.global().async {
            let jsonDictionary: [String: Any] = [
                "status": status,
                "notification": notification
            ]
            let jsonData = try! JSONSerialization.data(
                withJSONObject: jsonDictionary,
                options: []
            )
            
            do { try self.replyToClientSocket?.send(data: jsonData) }
            catch { print(error) }
            
            self.listenForRequest()
        }
    }
}

