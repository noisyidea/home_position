import UIKit
import SwiftyZeroMQ

class ClientVC: UIViewController {
    
    // MARK: - Stored Properties
    
    let context = try! SwiftyZeroMQ.Context()
    let pullEndpoint = "tcp://192.168.20.213:5555"
    let pushEndpoint = "tcp://192.168.20.213:9999"
    
    var serverNeedsMoreFrames = true
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        listenForPushes()
    }
    
    // MARK: - ZeroMQ
    
    private func listenForPushes() {
        DispatchQueue.global().async {
            do {
                let pullSocket = try self.context.socket(.pull)
                try pullSocket.connect(self.pushEndpoint)
                
                while true {
                    let message = try pullSocket.recv()
                    print(message)
                }
            } catch {
                print(error)
            }
        }
        
    }
    
    var timer = Timer?
    
    func performZeroMQTest() throws {
        let pushSocket = try context.socket(.push)
        try pushSocket.connect(pullEndpoint)
        
        let image = #imageLiteral(resourceName: "Boots")
        let imageData = UIImageJPEGRepresentation(image, 0.0)!
        
        print("Sending pushes")
        
        
        
        while serverNeedsMoreFrames {
            try pushSocket.send(data: imageData)
            
        }
        
        
//        print("Waiting for reply")
//        let reply = try requester.recv()
//        
//        print("Received reply: \(String(describing: reply))")
//        
//        try context.terminate()
    }
    
    // MARK: - Actions
    
    @IBAction func screenTapped(_ sender: Any) {
        do {
            try performZeroMQTest()
        } catch {
            print(error)
        }
    }
}

